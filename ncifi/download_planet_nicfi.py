from eumap import parallel
from eumap.misc import ttprint
from eumap.raster import read_rasters, save_rasters
from shapely.geometry import box
from pathlib import Path
import datetime
import geopandas as pd
import numexpr as ne
import numpy as np
import os
import rasterio
import traceback

# Compatible for v1
# pip install planet==1.5.2
#
# For migration check https://planet-sdk-for-python-v2.readthedocs.io/en/latest/get-started/upgrading/ 
from planet import api

def planet_nicfi_dates():
  today = datetime.date.today()
  max_year = int(today.strftime("%Y"))
  max_month = int(today.strftime("%m"))
  
  dates = [  '2020-10', '2020-11', '2020-12',]

  for year in range(2021, max_year + 1):
    mmax_month = 12 + 1
    if year == max_year:
      mmax_month = max_month

    for month in range(1,mmax_month):
      dates.append(f'{year}-{str(month).zfill(2)}')

  return dates

def planet_nicfi_urls(client, aoi, dates = None):
  result = {}

  if dates is None:
    dates = planet_nicfi_dates()

  for dt in dates:

    mosaic_name = f'planet_medres_normalized_analytic_{dt}_mosaic'
    print(mosaic_name)
    mosaic = client.get_mosaic_by_name(mosaic_name)
    mosaic = mosaic.response.json()
    mosaic = mosaic['mosaics'][0]
    
    bbox = tuple(aoi.total_bounds)
    disolved_aoi = aoi.dissolve().loc[0, 'geometry']
    
    quads = client.get_quads(mosaic, bbox=bbox)
    
    for page in quads.iter():
      for item in page.items_iter(50):
        intersects_aoi = box(*item['bbox']).intersects(disolved_aoi)
        if intersects_aoi:
          url = item['_links']['download']
          quad_id = item['id']
          if quad_id not in result:
            result[quad_id] = []
       
          result[quad_id].append({
            'raster_url': url,
            'date': dt,
            'bbox': bbox
          })
    
  return result

def read_data(raster_url, out_files):
    
  try:
    ttprint(f'Reading {raster_url}')

    raster_ds = rasterio.open(raster_url)
    band_data = raster_ds.read()
    band_data = band_data.astype('float32')

    mask = (band_data[4,:,:] == 1)

    band_data = band_data[0:4,:,:].transpose(1,2,0)
    band_data[mask] = np.nan

    expression = '(nir - red) / (nir + red)'
    params = { 'nir': band_data[:,:,3], 'red': band_data[:,:,2] }
    ndvi = ne.evaluate(expression, params, optimization='aggressive')
    ndvi = (ndvi * 100) + 100

    band_data = (band_data * 0.0001) * 200
    band_data = np.concatenate([band_data, np.stack([ndvi], axis=2)], axis=2)

    save_rasters(raster_url, out_files, band_data, nodata=255, dtype='uint8')
    return True

  except:
    traceback.print_exc()
    ttprint(f"ERROR: {raster_url}")
    return False

# Get one in https://www.planet.com/account/#/user-settings
API_KEY = '<API_KEY>'
VECTOR_FILE_AOI = '<GPKG_FILE>' # Single geometry
OUT_DIR = './planet_nicfi'

os.environ["PL_API_KEY"] = API_KEY
aoi = pd.read_file(VECTOR_FILE_AOI)
client = api.ClientV1()

planet_urls = planet_nicfi_urls(client, aoi)

Path(OUT_DIR).mkdir(exist_ok=True)

args = []
for key in planet_urls.keys():
  for r in planet_urls[key]:
    out_files = [ OUT_DIR + '/' + r['date'] + '/' + band + '/' + key + '.tif'
              for band in ['blue', 'green', 'red', 'nir', 'ndvi'] ]
    raster_url = r['raster_url']
    args.append((raster_url, out_files))

for a in parallel.job(read_data, args, n_jobs=10):
  continue
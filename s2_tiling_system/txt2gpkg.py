import pandas as pd
import geopandas as gpd
import os
from shapely.geometry import Polygon

# Only for https://hub.docker.com/r/opengeohub/pygeo-ide
#os.environ['PROJ_LIB'] = '/opt/conda/share/proj/'

url = 'https://hls.gsfc.nasa.gov/wp-content/uploads/2016/10/S2_TilingSystem2-1.txt'

df = pd.read_csv(url, sep="\s{1,}", header=0, engine='python')
df = df.rename(columns={
    'TilID': 'tile_id', 'Xstart': 'x_start', 'Ystart': 'y_start',
    'UZ': 'uz', 'EPSG': 'epsg', 
    'MinLon': 'xmin', 'MaxLon': 'xmax', 
    'MinLon.1': 'ymin', 'MaxLon.1': 'ymax'
})

geometries = []

for i,row in df.iterrows():
    epsg_id = row.loc['epsg']
    xmin, xmax, ymin, ymax  = float(row.loc['xmin']), float(row.loc['xmax']), float(row.loc['ymin']), float(row.loc['ymax'])
    geometry = Polygon([[xmin, ymin], [xmin, ymax], [xmax, ymax], [xmax, ymin]])
    geometries.append(geometry)
    
df['geometries'] = geometries
gdf = gpd.GeoDataFrame(df.sort_values('tile_id'), crs='epsg:4326', geometry="geometries")
gdf.to_file('./S2_TilingSystem2-1.gpkg')